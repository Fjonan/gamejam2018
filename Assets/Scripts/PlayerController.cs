﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class PlayerController : MonoBehaviour {

	//[HideInInspector]
	public List<GameObject>		NPCTrigger;									    // list of objects the player can pickup
	[Header("Behavior")]
	public float 				moveSpeed 		    = 6;							// how fast the player moves
	public float				sprintModifier 	    = 1.5f;							// when sprinting the moveSpeed gets multiplied by this
	public Color32              ColorDefault        = new Color32(46,103,0,255);    // unhidden basic player color
	public Color32              playerColor;                                        // current unaltered color of the player, can be changed by hiding place
	public Color32              playerReplColor;                                    // alternative replacement color set by color areas with special sprite shaders
	[Header("Status")]
	public bool					sprinting		    = false;						// currently running
	public bool 				moving			    = false;						// currently moving around
	public bool                 occupied            = false;                        // currently doing something (talking, lockpicking etc.) so move command wont register
	public GameObject           currentShape;

	private Rigidbody           _rigidbody             ;
	private SpriteRenderer	    _renderer              ;
	private GameController      _gamecntrl			   ;
	private Vector3 			velocity               ;
	private float				currentSpeed           ;
	private int                 currentShapeIndex   = 0;
	private int					myWorldStatus       = 0;

	void Start () 
	{
		_rigidbody        = GetComponent<Rigidbody>();
		_gamecntrl        = GameObject.FindGameObjectsWithTag("GameController")[0].GetComponent<GameController> ();
		NPCTrigger 	      = new List<GameObject>();

		updateShape       ( currentShape        );
		StartCoroutine    ( updatePlayerColor() );
	}

	void Update () 
	{
		if ( (Input.GetButton("Sprint") || Input.GetAxis("Sprint") < 0) && moving && !sprinting ){ 
			sprinting                       = true;
		} else if( Input.GetButtonUp("Sprint") || Input.GetAxis("Sprint") == 0 || !moving ){
			sprinting = false;
		}


		// Knock out enemies
		if ( Input.GetButtonDown("UseItem") && NPCTrigger.Count > 0 ) {
			foreach ( GameObject enemy in NPCTrigger ){
				if (_gamecntrl.worldStatus < 5) {
					_gamecntrl.worldStatus++;
				};
				GameObject.FindWithTag("MainCamera").GetComponent<CameraController>().ShakeCamera(0.25f);

			}
		}
	}

	void FixedUpdate() 
	{	
		// Move the player around
		if ( !occupied ){
			currentSpeed = sprinting ? moveSpeed * sprintModifier : moveSpeed;
			velocity 	 = new Vector3 (Input.GetAxisRaw ("Horizontal"), 0, Input.GetAxisRaw ("Vertical")).normalized * currentSpeed;
			moving 		 = !Vector3.Equals(velocity, new Vector3(0,0,0));
			_rigidbody.MovePosition (_rigidbody.position + velocity * Time.fixedDeltaTime);
		}
	}

	void OnTriggerEnter(Collider trigger) {
		switch (trigger.gameObject.tag) {
		case "NPC":
			if (!NPCTrigger.Contains (trigger.gameObject)) {
				// add to array of objects in range
				NPCTrigger.Add (trigger.gameObject);
			}
			break;
		}
	}	

	void OnTriggerExit(Collider trigger) {
		switch (trigger.gameObject.tag) {
		case "NPC":
			NPCTrigger.Remove (trigger.gameObject);
			break;
		}
	}

	/// <summary>
	/// Set player shape and update references
	/// </summary>
	private void updateShape( GameObject newShape )
	{
		currentShape.SetActive( false );
		newShape.SetActive( true );

		currentShape = newShape;
		_renderer    = currentShape.GetComponentInChildren<SpriteRenderer>();
	}

	/// <summary>
	/// Set Player Color by current status (color area, hideout) with a transition
	/// </summary>
	IEnumerator updatePlayerColor()
	{
		float 	duration 		 = .3f; 					// Time in seconds
		float 	progress 		 = 0; 						// 3rd parameter of the lerp function
		float 	smoothness 		 = 0.02f;					// Time between updates
		float 	increment 		 = smoothness/duration; 	// Amount of change to apply
		Color32 currentColor 	 = _renderer.color; 	    // State of the color in the current interpolation
		Color32 targetColor		 = playerColor;				// The color the player should have

		// Only do stuff if the colors differ
		if ( !currentColor.Equals(targetColor) ){
			while( progress < 1 )
			{
				Color.Lerp(currentColor    , targetColor    , progress);

				progress += increment;
				yield return new WaitForSeconds(smoothness);
			}
		} else {
			yield return new WaitForSeconds(smoothness);
		}

		StartCoroutine( updatePlayerColor() );
	}


}