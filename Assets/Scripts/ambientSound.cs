﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ambientSound : MonoBehaviour {
	public AudioClip[] srcLevels = new AudioClip[6];
	private int soundLevel = 0;
	private GameController _gamecntrl;

	void Start(){
		_gamecntrl        = GameObject.FindGameObjectsWithTag("GameController")[0].GetComponent<GameController> ();
		soundLevel = 0;
		GetComponent<AudioSource>().clip = srcLevels[soundLevel];
		GetComponent<AudioSource>().Play();
	}

	public void setSoundLevel (int level) {
		//GetComponent<AudioSource>().Stop();
		Debug.LogFormat("level {0}", level);
		GetComponent<AudioSource>().clip = srcLevels[level];
		GetComponent<AudioSource>().Play();
	} 
		
	void Update(){
		if (_gamecntrl.worldStatus != soundLevel && (soundLevel)  == (_gamecntrl.worldStatus -1))
		{
			soundLevel = _gamecntrl.worldStatus;

			setSoundLevel (soundLevel);
		}
	}
}
