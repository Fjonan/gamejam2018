﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharackterAnim : MonoBehaviour {

    public GameObject Level1_Anim;
    public GameObject Level2_Anim;
    public GameObject Level3_Anim;
    public GameObject Level4_Anim;
    public GameObject Level5_Anim;
    public GameObject Level0_Anim;

    private GameController _gamecntrl;
    private int myWorldStatus;


    void Start()
    {
        _gamecntrl = GameObject.FindGameObjectsWithTag("GameController")[0].GetComponent<GameController>();
    }

    void Update()
    {
        if (_gamecntrl.worldStatus == 0)
        {

            Level0_Anim.SetActive(true);
            Level1_Anim.SetActive(false);
            Level2_Anim.SetActive(false);
            Level3_Anim.SetActive(false);
            Level4_Anim.SetActive(false);
            Level5_Anim.SetActive(false);
        }

        if (_gamecntrl.worldStatus == 1)
            {
            Level0_Anim.SetActive(false);
            Level1_Anim.SetActive(true);
                Level2_Anim.SetActive(false);
                Level3_Anim.SetActive(false);
                Level4_Anim.SetActive(false);
                Level5_Anim.SetActive(false);
            }
            
            if (_gamecntrl.worldStatus == 2)
            {
            Level0_Anim.SetActive(false);
            Level2_Anim.SetActive(true);
                Level1_Anim.SetActive(false);
                Level3_Anim.SetActive(false);
                Level4_Anim.SetActive(false);
                Level5_Anim.SetActive(false);
            }
            
            if (_gamecntrl.worldStatus == 3)
            {
            Level0_Anim.SetActive(false);
            Level3_Anim.SetActive(true);
                Level1_Anim.SetActive(false);
                Level2_Anim.SetActive(false);
                Level4_Anim.SetActive(false);
                Level5_Anim.SetActive(false);
            }
            
            if (_gamecntrl.worldStatus == 4)
            {
            Level0_Anim.SetActive(false);
            Level4_Anim.SetActive(true);
                Level1_Anim.SetActive(false);
                Level2_Anim.SetActive(false);
                Level3_Anim.SetActive(false);
                Level5_Anim.SetActive(false);
            }

            if (_gamecntrl.worldStatus == 5)
            {
            Level0_Anim.SetActive(false);
            Level5_Anim.SetActive(true);
                Level1_Anim.SetActive(false);
                Level2_Anim.SetActive(false);
                Level3_Anim.SetActive(false);
                Level4_Anim.SetActive(false);
            }
            myWorldStatus = _gamecntrl.worldStatus;
        }

}


