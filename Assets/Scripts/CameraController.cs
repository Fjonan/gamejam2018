﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public GameObject   player;       //Public variable to store a reference to the player game object
	public Vector3      axisShakeMin;
	public Vector3      axisShakeMax;
	public float        timeOfShake;

	private float       timeOfShakeStore;
	private bool        shake               = false;
	private Vector3     startPos;
	private Vector3     offset;         // store the offset distance between the player and camera

	// Use this for initialization
	void Start () {
		//Calculate and store the offset value by getting the distance between the player's position and camera's position.
		offset           = transform.position - player.transform.position;

		timeOfShakeStore = timeOfShake;
	}

	void FixedUpdate () {
		if (shake) {         
			transform.position = startPos + new Vector3(Random.Range(axisShakeMin.x, axisShakeMax.x), Random.Range(axisShakeMin.y, axisShakeMax.y), Random.Range(axisShakeMin.z, axisShakeMax.z));
			timeOfShake -= Time.deltaTime;
			if (timeOfShake <= 0.0f) {
				shake               = false;
				timeOfShake         = 0;
				transform.position  = startPos;
			}        
		}
	}

	public void ShakeCamera(float shakeTime = -1.0f) {
		if (shakeTime > 0.0f) {
			timeOfShake = shakeTime;
		} else {
			timeOfShake = timeOfShakeStore;
		}
		shake       = true;
		startPos    = transform.position;
	}

	// LateUpdate is called after Update each frame
	void LateUpdate () {
		if ( !shake ) {
			// Set the position of the camera's transform to be the same as the player's, but offset by the calculated offset distance.
			transform.position = player.transform.position + offset;
		}
	}
}