﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldObject : MonoBehaviour {

	public GameObject[] objectStatus;
	public string name;

	private GameController _gamecntrl;
	private int			   myWorldStatus;

    // Use this for initialization
    void Start () {
		_gamecntrl        = GameObject.FindGameObjectsWithTag("GameController")[0].GetComponent<GameController> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (_gamecntrl.worldStatus != myWorldStatus && objectStatus.Length >= _gamecntrl.worldStatus)
		{
			objectStatus [myWorldStatus].SetActive (false);
			objectStatus [_gamecntrl.worldStatus].SetActive (true);

			string spritename = name + _gamecntrl.worldStatus;
			Sprite sprite = Resources.Load (spritename, typeof(Sprite)) as Sprite;
			SpriteRenderer cursprite = objectStatus [_gamecntrl.worldStatus].GetComponent<SpriteRenderer> ();
            
			cursprite.sprite = sprite;
            myWorldStatus = _gamecntrl.worldStatus;
        }
	}
}
