﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteFlip : MonoBehaviour {

    public Rigidbody Entity;
    public SpriteRenderer Visual;
	
	// Update is called once per frame
	void Update () {
        if (Entity.velocity.x == 0)
        {
            Debug.Log("Idle");
        }
        if (Entity.velocity.x <= 0.1)
        {
            Visual.flipX = true;
            Debug.Log("Left");
        }
        if (Entity.velocity.x >= 0.1)
        {
            Visual.flipX = false;
            Debug.Log("Right");
        }
    }
}
