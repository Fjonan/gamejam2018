﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour {

	public Animator titlescreen;

	private bool playerHere = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (playerHere) {
			foreach (Transform child in titlescreen.transform) {
				child.gameObject.SetActive (true);
			}
			titlescreen.SetBool ("fade", true);
		}
	}

	void OnTriggerEnter(Collider trigger) {
		switch (trigger.gameObject.tag) {
		case "Player":
			playerHere = true;
			break;
		}
	}	

	void OnTriggerExit(Collider trigger) {
		switch (trigger.gameObject.tag) {
		case "Player":
			playerHere = false;
			break;
		}
	}
}
